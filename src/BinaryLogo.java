import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

/**
 * Takes a binary image specified by input.png and produces an image of ones and
 * zeroes of different colors (binary.png).
 */
public class BinaryLogo {

	// Dimensions of output image
	private static final int OUT_WIDTH = 2000;

	private static final int FONT_SIZE = 24;
	private static final Font FONT = new Font("CourierNew", Font.BOLD,
			FONT_SIZE);

	private static final String INPUT_FILENAME = "input.png";
	private static final String OUTPUT_FILENAME = "binary.png";

	public static void main(String[] args) throws IOException {

		// Setting up input
		BufferedImage inImg = ImageIO.read(new File(INPUT_FILENAME));

		// Setting up output
		int outHeight = (int) Math.round(1.0 * inImg.getHeight()
				/ inImg.getWidth() * OUT_WIDTH);
		BufferedImage outImg = new BufferedImage(OUT_WIDTH, outHeight,
				BufferedImage.TYPE_3BYTE_BGR);
		Graphics2D g2 = outImg.createGraphics();
		g2.setFont(FONT);

		// Ensuring that text is drawn smoothly (antialiased)
		g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
				RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

		// The output image space is divided into boxes of equal height and
		// width each containing one bit
		double boxHeight = 1.0 * outHeight / inImg.getHeight();
		double boxWidth = 1.0 * OUT_WIDTH / inImg.getWidth();

		for (int x = 0; x < inImg.getWidth(); x++) {
			for (int y = 0; y < inImg.getHeight(); y++) {

				g2.setColor(new Color(inImg.getRGB(x, y)));

				// Draw bit to centre of box
				drawCentredString(Math.random() < 0.5 ? "1" : "0",
						x * boxWidth, y * boxHeight, boxWidth, boxHeight, g2);
			}

		}

		// Finish up IO
		ImageIO.write(outImg, "png", new File(OUTPUT_FILENAME));
	}

	/**
	 * Draws the string s in the centre of the rectangle with top-left corner
	 * (x, y) and dimensions (width, height) on the given Graphics2D.
	 */
	private static void drawCentredString(String s, double x, double y,
			double width, double height, Graphics2D g2) {
		FontMetrics fontMetrics = g2.getFontMetrics();
		Rectangle2D stringBounds = fontMetrics.getStringBounds(s, g2);
		g2.drawString(
				s,
				(int) Math.round(width / 2 - stringBounds.getWidth() / 2 + x),
				(int) Math.round(height / 2 - stringBounds.getHeight() / 2 + y
						+ fontMetrics.getAscent()));
	}
}